# **GUI Good Practices**

>A graphical user interface is what a user sees and interects with. Designing a user interface that is easy-to-unerstand yet accomplishes the necessary tasks is critical. 

>Folowing are the points for best GUI practices:

### 1. Keep the interface simple
> The best interfaces are almost invisible to the user. They avoid unnecessary elements and are clear in the language they use on labels and in messaging. [UI design basics]
### 2. Create consistency and use common GUI elements
>  By using common elements in UI, users feel more comfortable and are able to get things done more quickly. It is also important to create patterns in language, layout and design throughout the site to help facilitate efficiency. [UI design basics]
### 3. Be purposeful in page layout
> Consider the spatial relationships between items on the page and structure the page based on importance. Careful placement of items can help draw attention to the most imporatnt pieces of information and can aid scanning and readibility. [UI design basics]
### 4. Strategically use color and texture
> You can direct attention toward or redirect attention away from items using color, light, contrast and texture to your advantage. [UI design basics]
### 5. Make good use of typography
> Another elegant way to establish a visual hierarcy is through the use of typography. For starters, every font has a personality. Size, of course, plays a key role when using fonts in design. Important information such as headers are highlighted.  [10 rules of good UI design] 
### 6. Be forgiving
> No matter how clear your design is, people will make mistakes. your UI should allow for and tolerate user error. Design ways for user to undo actions, and be forgiving with varied inputs. [UI design fundamentals] 
### 7. Help!
> An interface should provide meaningful feedback that describes the state of the system to users. If an error occurs, users should be notified and informed of ways to recover. It should also provide a comrehensive help system. This can be done with inline help, a support database or a knowledge base tour. [UI in business web application design]
### 8. Collaborate your way to success
> Depending on the scope of your project, at some point you might find yourself working as part of a team that can shoulder some of the work alongside you, and in such a scenario it's crucial to make sure that all members of the team share the same information in order to create a usable and appealing interface. [10 rules of good UI design]


# Conclusion
> Designing GUI is a challenging job that is full of compromises between client and user needs. It requires a solid understanding of users and their tasks, as well as of UI design principles and patterns. 


[UI in business web application design]:(www.smashingmagazine.com/2010/02/designing-user-interfaces-for-business-web-applications/)
[UI design fundamentals]:(http://blog.teamtreehouse.com/10-user-interface-design-fundamentals)

[UI design basics]:(https://www.usability.gov/what-and-why/user-interface-design.html)

[10 rules of good UI design]:(https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project)





<br><br>


## **PEER REVIEW BY STEPHEN HEWLETT** &nbsp; Date: 10/8/17

Guri has located some key online sources for principles of good UI design. &nbsp; His report demonstrates awareness of the many practices that a professional programmer needs to adopt when devising a new app or webpage.

There is a tendency to rely too heavily on the wording of his source material. &nbsp; More time could be put into presenting the principles in his own words, which would better demonstrate his own understanding of the material. &nbsp; This is particularly evident where his text contains multiple principles or concepts that need unpacking. &nbsp; For example, his two sentences for Principle 2 contain references to familiarity, consistency, and efficiency, each of which could have their own paragraph.

More refinement is needed in his citations. &nbsp; To comply with APA Referencing each citation for a direct quote should appear in brackets, with a date if available.  &nbsp; Speech marks are required for direct quotes, such as his use of the text verbatim from the article _UI Design Fundamentals_ in his Principle 6, or from _UI Design Basics_ in his Principle 4.

In Markdown, the block text feature is best reserved for block quoting.  Guri has chosen to use this throughout his report. &nbsp; It is effective in providing visual breaks in the material, but breaks between paragraphs can be managed in other ways.

Overall, the report is a succinct and useful starting point for the novice programmer starting to think about best practices in UI programming.